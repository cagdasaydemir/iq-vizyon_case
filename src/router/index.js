import { createRouter, createWebHashHistory } from 'vue-router'
import MachinesView from '../views/MachinesView.vue'
import DashboardView from '../views/DashboardView.vue'


const routes = [
  { path: '/', redirect: '/machines' },
  {
    path: '/machines',
    name: 'machines',
    component: MachinesView
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: DashboardView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
