import { createStore } from "vuex";
import machines from "./modules/machines";
// import createPersistedState from 'vuex-persistedstate'
// those commented lines created for storing vuex data after refreshing page but its not working

export const store = createStore({
  state: {},
  getters: {},
  actions: {},
  mutations: {},
  modules: { machines },
  // plugins: [createPersistedState()],
});
