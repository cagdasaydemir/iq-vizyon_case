import axios from "axios";

const state = {
  machines: [],
  board: [],
  currentView: "",
};
const getters = {
  allMachines: (state) => state.machines,
  allBoard: (state) => state.board,
  currentView: (state) => state.currentView,
};
const actions = {
// // FIX THIS
//   async createWebSocket() {
//     const Websocket = require("ws");
//     console.log("Starting connection to WebSocket Server");
//     this.connection = new WebSocket("ws://192.168.1.34:8080/ws");

//     this.connection.onopen = function (event) {
//       console.log("Successfully connected to the websocket server...");
//     };
//     this.connection.onmessage = function (event) {
//       console.log(event.data);
//     };
//     this.connection.onerror = function (event) {
//       console.log("error");
//     };
//   },
  async fetchMachines({ commit }) {
    const res = await axios.get("http://localhost:5000/machines/");
    res.data.forEach((machine) => {
      machine.boardStatus = false;
    });
    commit("setMachines", res.data);
  },
  async toggleBoard({ commit, dispatch }, id) {
    await commit("toggleBoard", id - 1);
    dispatch("setBoard");
    
    // // FIX THIS => if all machines removed after setting curremtView, dashboard page crashes.
    // if(state.machines[id-1].id == state.currentView.id){
    //     await commit("setDetailView", state.board[0]);
    // }else if (state.board == []){
    //         dispatch("resetDetailView()")
    // }
  },
  async setBoard({ commit }) {
    const currentBoard = [];
    state.machines.map((machine) => {
      if (machine.boardStatus === true) currentBoard.push(machine);
    });
    await commit("setBoard", currentBoard);
  },
  async setDetailView({ commit }, bodyId) {
    let currentView;
    state.board.filter((machine) => {
      if (machine.id === bodyId) currentView = machine;
    });
    await commit("setDetailView", currentView);
  },
  async resetDetailView({ commit }) {
    const currentView = false;
    await commit("setDetailView", currentView);
  },
};
const mutations = {
  setMachines: (state, machines) => (state.machines = machines),
  toggleBoard: (state, id) =>
    (state.machines[id].boardStatus = !state.machines[id].boardStatus),
  setBoard: (state, machines) => (state.board = machines),
  setDetailView: (state, machine) => (state.currentView = machine),
};

export default {
  state,
  getters,
  actions,
  mutations,
};
